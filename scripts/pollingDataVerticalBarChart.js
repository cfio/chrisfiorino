function pollingDataVerticalBarChart() {

d3 = d3v3;

// Color Pallete
var lightpurple = "#B895FF";
var purple = "#78DBFF";
var bluegrey = "#545C72";
var darkaqua = "#1797A5";
var aqua = "#03A5A5";

// Create data variable

var dataset = [
                {'val':2,'key':'Clinton'},
                {'val':5,'key':'Trump'},
                {'val':8,'key':'Sanders'},
                {'val':19,'key':'Kasich'},
                {'val':22,'key':'Rubio'},
                {'val':27,'key':'Cruz'},
                {'val':33,'key':'Carson'},
                {'val':35,'key':'Bush'},
                {'val':42,'key':'Fiorina'}];
    

// Fill variable with dummy data


var margin = { top: 30, right: 30, bottom: 40, left:50 }

var height = 500,
    width = 960,
    barWidth = 50,
    barOffset = 5;

var yScale = d3.scale.linear()
        .domain([0, d3.max(dataset, function(d) {return d.val;})])
        .range([height - 10, 0])

var xScale = d3.scale.ordinal()
        .domain(dataset.map(function (d) {return d.key;}))
        .rangeBands([0, width], 0.2)
/*
var tooltip = d3.selectAll('.tooltip')
var SVGtooltip = d3.select('g.tooltip')*/
      

var tempColor;

d3.selection.prototype.moveToFront = function() {
  return this.each(function(){
  this.parentNode.appendChild(this);
  });
};

    /*

d3.select('#d3')
        .append('div')
        .attr('class', 'tooltip')
        .style('fill', 'red')
        .text('HTML Tip')
     .attr('width', '50px')
     .attr('height','50px')

    */


// Vertical Bar Chart Visualization Function

    
  /*  
    var tooltip = d3.select('body').append('div')
        .style('position', 'absolute')
        .style('padding', '0 10px')
        .style('background', 'white')
        .style('opacity', 0)
    */
    
var myChart = d3.select('#modalSvg')
                .append('g')
                    .attr('id', 'box')
                    .attr('transform', 'translate(100,50) scale(0.8)')
                    .style('background', purple)
                .append('g')

                .selectAll('rect.data').data(dataset)
                        .enter().append('rect')
                            .style('fill', purple)
                            .attr('class', 'data')
                            .attr('width', xScale.rangeBand())
                            .attr('height', function(d){
                                return yScale(d.val);
                            })
                            .attr('x', function(d,i) {
                                return xScale(d.key);
                            })
                            .attr('y', function(d) {
                                return height - yScale(d.val);
                            })



        /*
    .on("mousemove", function () {
        console.log('mouseover 2');
        

var HTMLtooltip = d3.select('div.tooltip')
    
        HTMLtooltip
            //.style('fill', darkaqua)
            .style("top", (d3.event.pageY) + "px")
            .style("left", (d3.event.pageX) + "px")
            
            //.moveToFront();
    })
    */

//Somewhat working Tooltip Start
/*
    .on('mouseover', function() {
        tooltip.transition()
            .style('opacity', .9)

        tooltip.html(bardata)
            .style('left', (d3.event.pageX - 35) + 'px')
            .style('top',  (d3.event.pageY - 30) + 'px')
    })
*/
//Somewhat working Tooltip End
/*
    .on("mouseover", function() {
        console.log('mouseover 1');
        return g.tooltip.style('visibility', 'visible');
    })

    .on("mouseout", function() {
        console.log('mouseout 3');
        return g.tooltip.style("visibility", "hidden");
    })
    
   */ 
    .on('mouseover', function(d) {
    
            tempColor = this.style.fill;
            d3.select(this)
                .style('opacity', 1)
                .style('fill', lightpurple)
    
            //div.transition()
//				.duration(200)	
//				.style("opacity", .9);	
    
			//div.html(d) 
//				.style("left", (d3.event.pageX) + "px")			 
//				.style("top", (d3.event.pageY) + "px")
            
            //tooltip.html(d)
            //    .style('left', (d3.event.pageX - 35) + 'px')
            //    .style('top', (d3.event.pageY - 30) + 'px')
    
            
            
            
                
        })
    
        .on('mouseout', function(d) {
    
            d3.select(this)
                .style('opacity', 1)
                .style('fill', tempColor)
    
            //div.transition()
            //    .duration(500)	
            //.style("opacity", 0);
    
            
    
            
        })
    
/*
    d3.select('#box').append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '18%')
        .attr('height', '40%')
        .style('fill', aqua)
        .attr('x', '750px')
        .attr('y', '30px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    d3.select('#box').append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '40px')
        .style('fill', lightpurple)
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    d3.select('#box').append('text')
        .attr('x', '818px')
        .attr('y', '57px')
        .text('Light Purple')
        .style('font-size', '10pt')
    
    d3.select('#box').append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '80px')
        .style('fill', purple)
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    d3.select('#box').append('text')
        .attr('x', '818px')
        .attr('y', '97px')
        .text('Purple')
        .style('font-size', '10pt')
    
    */
    
    var xAxis = d3.svg.axis()
        .scale(xScale)
        .orient('bottom');
    
    d3.select('#box').append('g')
        .attr('class', 'axis-x')
        .attr('transform', 'translate(0, 500)')
        .call(xAxis)
            .append('text')
            .text('Presidential Candidates')
            .attr('transform', 'translate(400,40)')
            .style('font-size', '16px');
    
    var yAxis = d3.svg.axis()
        .scale(yScale)
        .orient('left')
        .ticks(10)
        
    d3.select('#box').append('g')
        .attr('class', 'axis-y')
        .attr('transform', 'translate(0,7)')
        .call(yAxis)
            .append('text')
            .text('Polling %')
            .attr('transform', 'translate(-40,275) rotate(-90)')
            .style('font-size', '16px')

    
    d3.select('#modalSvg').append('g')
        .append('text')
        
d3.select('#modalSvg').append('text')
                .text('National Candidate Approval')
                .attr('transform', 'translate(355,20)')
                .style('font-size', '16px')
                .style('fill', 'white')
    
    
 myChart.transition()
        .attr('height', function(d) {
            return yScale(d.val);
        })
        .attr('y', function(d) {
            return height - yScale(d.val);
        })
        .delay(function(d, i) {
            return i * 20;
        })
        .duration(1000)
        .ease('elastic')

    /*
    d3.select('#d3').append('div')
        .attr('class', 'vertBarLegend')
        .text("this is a div")
    */
    
//Scale for axes
/*
    var vGuideScale = d3.scale.linear()
        .domain([0, d3.max(bardata)])
        .range([height, 0])
    
    var vAxis = d3.svg.axis()
        .scale(vGuideScale)
        .orient('left')
        .ticks(10)
    
    var vGuide = d3.select('svg').append('g')
    vAxis(vGuide)
    vGuide.attr('transform', 'translate(' + margin.left + ', ' + margin.top + ')')
    vGuide.selectAll('path')
        .style({ fill: 'none', stroke: "#000"})
    vGuide.selectAll('line')
        .style({ stroke: "#000"})
    
    */
    
}



    
//margin - top right bottom left
//width of overall viz
//height of overall viz
//xAxis - orientation, ticks, scale
//yAxis - orientation, ticks, scale
//xScale - linear or ordinal + domain and range
//yScale - linear or ordinal + domain and range
//svg - appends parent element and is appended by g
//g - appends svg element and represents data points
//tsv - brings in data from document
//text - appends g to add text
//style - inline styling of elements
//selectAll - selects all elements
//enter - used after a selectAll to create new elements that will be retroactively selected and modified
//vAxis - vertical axis for displaying scale
//hAxis - horizontal axis for displaying scale

    
