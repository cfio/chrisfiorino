function textAnalysisWordCloud() {
    
d3 = d3v3;

var frequency_list = [
{"text":"AnalytixBar","size":80},
{"text":"work","size":35},
{"text":"business","size":62},
{"text":"client","size":11},
{"text":"team","size":70},
{"text":"microstrategy","size":9},
{"text":"help","size":10},
{"text":"data","size":10},
{"text":"service","size":7},
{"text":"llc","size":45},
{"text":"analytics","size":5},
{"text":"cloud","size":5},
{"text":"hire","size":15},
{"text":"solution","size":5},
{"text":"power","size":5},
{"text":"concrete","size":10},
{"text":"car","size":10},
{"text":"best","size":5},
{"text":"datastrong","size":35},
{"text":"right","size":5},
{"text":"company","size":10},
{"text":"motor","size":10},
{"text":"driver","size":10},
{"text":"creative","size":5},
{"text":"create","size":45},
{"text":"visit","size":40},
{"text":"design","size":30},
{"text":"website","size":35},
{"text":"new","size":40},
{"text":"engineering","size":10},
{"text":"software","size":5},
{"text":"intelligence","size":45},
{"text":"home","size":15},
{"text":"implementation","size":15},
{"text":"provide","size":15},
{"text":"lead","size":15},
{"text":"future","size":15},
{"text":"customer","size":15},
{"text":"experience","size":15},
{"text":"dashboard","size":15},
{"text":"tool","size":15},
{"text":"train","size":15},
{"text":"report","size":15},
{"text":"specialized","size":35},
{"text":"provider","size":5},
{"text":"app","size":15},
{"text":"application","size":5},
{"text":"expert","size":10},
{"text":"custom","size":5},
{"text":"enterprise","size":5},
{"text":"offer","size":5},
{"text":"order","size":10},
{"text":"believe","size":10},
{"text":"deploy","size":10},
{"text":"modify","size":5},
{"text":"add","size":10},
{"text":"auto","size":5},
{"text":"automate","size":5},
{"text":"product","size":5},
{"text":"fast","size":5},
{"text":"benefit","size":5},
{"text":"top","size":5},
{"text":"skill","size":5},
{"text":"manage","size":5},
{"text":"meet","size":5},
{"text":"professional","size":5},
{"text":"develop","size":5},
{"text":"analysis","size":5},
{"text":"operation","size":5},
{"text":"consulting","size":5},
{"text":"build","size":5},
{"text":"washington","size":5},
{"text":"track","size":5},
{"text":"trend","size":10},
{"text":"find","size":10},
{"text":"ensure","size":10},
{"text":"media","size":10},
{"text":"improve","size":10},
{"text":"information","size":10},
{"text":"delivery","size":10},
{"text":"recommend","size":5},
{"text":"unique","size":5},
{"text":"optimal","size":10},
{"text":"configure","size":5},
{"text":"tailor","size":5},
{"text":"system","size":5},
{"text":"story","size":5},
{"text":"high-value","size":5},
{"text":"platform","size":5},
{"text":"reduce","size":5},
{"text":"enable","size":5},
{"text":"insight","size":5},
{"text":"career","size":5},
{"text":"locate","size":5},
{"text":"average","size":5},
{"text":"centralize","size":5},
{"text":"creation","size":35},
{"text":"model","size":35},
{"text":"record","size":5},
{"text":"practice","size":5},
{"text":"very","size":5},
{"text":"support","size":35},
{"text":"brand","size":5},
{"text":"nice","size":5},
{"text":"recruit","size":5},
{"text":"expertise","size":5},
{"text":"beauty","size":5},
{"text":"huge","size":10},
{"text":"market","size":10},
{"text":"heart","size":5},
{"text":"know","size":5},
{"text":"leverage","size":5},
{"text":"accessible","size":5},
{"text":"integration","size":5},
{"text":"love","size":5}];


    var color = d3.scale.linear()
            .domain([0,1,2,3,4,5,6,10,15,20,100])
            .range(["#FFFFFF", "#f57c00", "#e65100", "#ffcc80", "#ffb74d", "#f57c00", "#CC352B", "#FF4236", "#CC352B", "#ffcc80", "#fb8c00", "#ffb74d"]);

    d3.layout.cloud().size([960, 500])
            .words(frequency_list)
            .rotate(0)
            .fontSize(function(d) { return d.size; })
            .on("end", draw)
            .start();

    function draw(words) {
        d3.select("#modalSvg").append("svg")
                .attr("width", "960px")
                .attr("height", "500px")
                .attr("class", "wordcloud")
                .append("g")
                // without the transform, words words would get cutoff to the left and top, they would
                // appear outside of the SVG area
                .attr("transform", "translate(370,200) scale(.8)")
                .selectAll("text")
                .data(words)
                .enter().append("text")
                .style("font-size", function(d) { return d.size + "px"; })
                .style("fill", function(d, i) { return color(i); })
                .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .text(function(d) { return d.text; });
        
         d3.select('#modalSvg').append('text')
                .text('AnalytixBar Website Word Frequency')
                .attr('transform', 'translate(345,20)')
                .style('font-size', '16px')
                .style('fill', 'white')
        
    }
    
    
    
}
