function contentImpactTreemap() {
    
d3 = d3v3;
    
var width = 960,
    height = 500,
    color = d3.scale.ordinal()
    .range([   '#3A7F51',
               '#16647F',
               '#42247F',
               '#7F211B',
               '#CC352B'
               ]);

var treemap = d3.layout.treemap()
    .padding(4)
    .size([width, height])
    .value(function(d) { return d.size; });

var svg = d3.select("#modalSvg").append("svg")
    .attr("width", width)
    .attr("height", height)
  .append("g")
    .attr("transform", "translate(40,50) scale(0.8)");

d3.json("data/treemap.json", function(error, json) {
  if (error) throw error;

  var cell = svg.data([json]).selectAll("g")
      .data(treemap.nodes)
    .enter().append("g")
      .attr("class", "cell")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

  cell.append("rect")
      .attr('class', 'treeRect')
      .attr("width", function(d) { return d.dx; })
      .attr("height", function(d) { return d.dy; })
      .style("fill", function(d) { return d.children ? color(d.name) : null; });

  cell.append("text")
      .attr("x", function(d) { return d.dx / 2; })
      .attr("y", function(d) { return d.dy / 2; })
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .style("fill", "white")
      .text(function(d) { return d.children ? null : d.name; });
    
    d3.select('#modalSvg').append('text')
                .text('Webpage Click Count by Element')
                .attr('transform', 'translate(340,20)')
                .style('font-size', '16px')
                .style('fill', 'white')
    
 var legendGroup = d3.select('#modalSvg').append('g')
                .attr('transform', 'translate(300,70) scale(0.7)');
    
    legendGroup.append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '19%')
        .attr('height', '41%')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('x', '750px')
        .attr('y', '30px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '40px')
        .style('fill', '#16647F')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '57px')
        .text('Home')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '80px')
        .style('fill', '#42247F')
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '97px')
        .text('Work')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '120px')
        .style('fill', '#CC352B')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '137px')
        .text('Visualize')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '160px')
        .style('fill', '#7F211B')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '177px')
        .text('About')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '200px')
        .style('fill', '#3A7F51')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '217px')
        .text('Contact')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('text')
        .attr('x', '808px')
        .attr('y', '18px')
        .text('Webpage')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
var sizeLegendGroup = d3.select('#modalSvg').append('g')
                .attr('transform', 'translate(300,250) scale(0.7)');
    
    sizeLegendGroup.append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '19%')
        .attr('height', '41%')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('x', '750px')
        .attr('y', '30px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    sizeLegendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '40px')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .attr('width', '160px')
        .attr('height', '25px')
        .attr('rx', '2px')
        .attr('ry', '2px')
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
    
    sizeLegendGroup.append('text')
        .attr('x', '812px')
        .attr('y', '57px')
        .text('250 clicks')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    sizeLegendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '70px')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('width', '160px')
        .attr('height', '50px')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    sizeLegendGroup.append('text')
        .attr('x', '812px')
        .attr('y', '99px')
        .text('500 clicks')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    sizeLegendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '125px')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .attr('width', '160px')
        .attr('height', '100px')
        .attr('rx', '2px')
        .attr('ry', '2px')
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
    
    sizeLegendGroup.append('text')
        .attr('x', '808px')
        .attr('y', '177px')
        .text('1000 clicks')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    sizeLegendGroup.append('text')
        .attr('x', '808px')
        .attr('y', '20px')
        .text('# of Clicks')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')

    
});

}
