function pollingDataStackedBarChart() {
    
d3 = d3v3;

var n = 4, // number of layers
    m = 52, // number of samples per layer
    stack = d3.layout.stack(),
    layers = stack(d3.range(n).map(function() { return bumpLayer(m, .1); })),
    yGroupMax = d3.max(layers, function(layer) { return d3.max(layer, function(d) { return d.y; }); }),
    yStackMax = d3.max(layers, function(layer) { return d3.max(layer, function(d) { return d.y0 + d.y; }); });

var margin = {top: 40, right: 10, bottom: 20, left: 10},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var x = d3.scale.ordinal()
    .domain(d3.range(m))
    .rangeRoundBands([0, width], .08);

var y = d3.scale.linear()
    .domain([0, yStackMax])
    .range([height, 0]);

var color = d3.scale.ordinal()
    .range(['#5C4B7F','#5CCC81','#CC352B','#239FCC']);

var xAxis = d3.svg.axis()
    .scale(x)
    .tickSize(0)
    .tickPadding(6)
    .orient("bottom");
    
var yAxis = d3.svg.axis()
    .scale(y)
    .tickSize(0)
    .tickPadding(6)
    .orient("left");

var svg = d3.select("#modalSvg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(120,90) scale(0.7)");

var layer = svg.selectAll(".layer")
    .data(layers)
  .enter().append("g")
    .attr("class", "layer")
    .style("fill", function(d, i) { return color(i); });

var rect = layer.selectAll("rect")
    .data(function(d) { return d; })
  .enter().append("rect")
    .attr("x", function(d) { return x(d.x); })
    .attr("y", height)
    .attr("width", x.rangeBand())
    .attr("height", 0);

rect.transition()
    .delay(function(d, i) { return i * 10; })
    .attr("y", function(d) { return y(d.y0 + d.y); })
    .attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); });

svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
        .append('text')
            .text('Week')
            .attr('transform', 'translate(440,40)')
            .style('font-size', '14px');
    
svg.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate(0,0)")
    .call(yAxis)
        .append('text')
            .text('Vote %')
            .attr('transform', 'translate(-30,240) rotate(-90)')
            .style('font-size', '14px');;

d3.selectAll("input").on("change", change);

var timeout = setTimeout(function() {
  d3.select("input[value=\"grouped\"]").property("checked", true).each(change);
}, 2000);

function change() {
  clearTimeout(timeout);
  if (this.value === "grouped") transitionGrouped();
  else transitionStacked();
}

function transitionGrouped() {
  y.domain([0, yGroupMax]);

  rect.transition()
      .duration(500)
      .delay(function(d, i) { return i * 10; })
      .attr("x", function(d, i, j) { return x(d.x) + x.rangeBand() / n * j; })
      .attr("width", x.rangeBand() / n)
    .transition()
      .attr("y", function(d) { return y(d.y); })
      .attr("height", function(d) { return height - y(d.y); });
}

function transitionStacked() {
  y.domain([0, yStackMax]);

  rect.transition()
      .duration(500)
      .delay(function(d, i) { return i * 10; })
      .attr("y", function(d) { return y(d.y0 + d.y); })
      .attr("height", function(d) { return y(d.y0) - y(d.y0 + d.y); })
    .transition()
      .attr("x", function(d) { return x(d.x); })
      .attr("width", x.rangeBand());
}
   
var legendGroup = d3.select('#modalSvg').append('g')
                .attr('transform', 'translate(340,130) scale(0.6)');    
    
legendGroup.append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '19%')
        .attr('height', '33%')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('x', '750px')
        .attr('y', '30px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '40px')
        .style('fill', '#239FCC')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '57px')
        .text('Democrat')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '80px')
        .style('fill', '#CC352B')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '97px')
        .text('Republican')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '120px')
        .style('fill', '#5CCC81')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '137px')
        .text('Independent')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '160px')
        .style('fill', '#5C4B7F')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '177px')
        .text('Undecided')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
        d3.select('#modalSvg').append('text')
                .text('Polling data by Week')
                .attr('transform', 'translate(390,20)')
                .style('font-size', '16px')
                .style('fill', 'white')

// Inspired by Lee Byron's test data generator.
function bumpLayer(n, o) {

  function bump(a) {
    var x = 1 / (.1 + Math.random()),
        y = 2 * Math.random() - .5,
        z = 10 / (.1 + Math.random());
    for (var i = 0; i < n; i++) {
      var w = (i / n - y) * z;
      a[i] += x * Math.exp(-w * w);
    }
  }

  var a = [], i;
  for (i = 0; i < n; ++i) a[i] = o + o * Math.random();
  for (i = 0; i < 5; ++i) bump(a);
  return a.map(function(d, i) { return {x: i, y: Math.max(0, d)}; });
}
    
}
