$(document).ready(function() {
$('#comparisonInfoTip').mouseenter(function(){
    console.log('enter');
    var pos = $('#comparisonInfoTip').position();
    $(this).css({"background": "rgba(255,255,255,0.5)"});
    $('#comparisonTooltip').css('top', (pos.top)-91 + 'px').fadeIn();
    
}).mouseleave(function(){
    console.log('exit');
    $(this).css({"background": "rgba(255,255,255,0.2)"});
    $('#comparisonTooltip').fadeOut();
});
    
$('#distributionInfoTip').mouseenter(function(){
    console.log('enter');
    var pos = $('#distributionInfoTip').position();
    $(this).css({"background": "rgba(255,255,255,0.5)"});
    $('#distributionTooltip').css('top', (pos.top)-105 + 'px').fadeIn();
    
}).mouseleave(function(){
    console.log('exit');
    $(this).css({"background": "rgba(255,255,255,0.2)"});
    $('#distributionTooltip').fadeOut();
});
    
$('#compositionInfoTip').mouseenter(function(){
    console.log('enter');
    var pos = $('#compositionInfoTip').position();
    $(this).css({"background": "rgba(255,255,255,0.5)"});
    $('#compositionTooltip').css('top', (pos.top)-91 + 'px').fadeIn();
    
}).mouseleave(function(){
    console.log('exit');
    $(this).css({"background": "rgba(255,255,255,0.2)"});
    $('#compositionTooltip').fadeOut();
});
    
$('#relationshipInfoTip').mouseenter(function(){
    console.log('enter');
    var pos = $('#relationshipInfoTip').position();
    $(this).css({"background": "rgba(255,255,255,0.5)"});
    $('#relationshipTooltip').css('top', (pos.top)-105 + 'px').fadeIn();
    
}).mouseleave(function(){
    console.log('exit');
    $(this).css({"background": "rgba(255,255,255,0.2)"});
    $('#relationshipTooltip').fadeOut();
});
    
});
