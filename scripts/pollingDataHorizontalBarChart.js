function pollingDataHorizontalBarChart() {
    
d3 = d3v3

var m = [30, 10, 10, 30],
    w = 960 - m[1] - m[3],
    h = 500 - m[0] - m[2];

var format = d3.format(",.0f");

var x = d3.scale.linear().range([0, w]),
    y = d3.scale.ordinal().rangeRoundBands([0, h], .1);

var xAxis = d3.svg.axis().scale(x).orient("bottom").tickSize(-h),
    yAxis = d3.svg.axis().scale(y).orient("left").tickSize(0);

var svg = d3.select("#modalSvg").append("svg")
    .attr("width", w + m[1] + m[3])
    .attr("height", h + m[0] + m[2])
  .append("g")
    .attr("transform", "translate(70,50) scale(0.9)");

d3.csv("data/horizontalBarChartData.csv", function(error, data) {
  if (error) throw error;

  // Parse numbers, and sort by value.
  data.forEach(function(d) { d.value = +d.value; });
  data.sort(function(a, b) { return b.value - a.value; });

  // Set the scale domain.
  x.domain([0, d3.max(data, function(d) { return d.value; })]);
  y.domain(data.map(function(d) { return d.name; }));

  var bar = svg.selectAll("g.bar")
      .data(data)
    .enter().append("g")
      .attr("class", "bar")
      .attr("transform", function(d) { return "translate(0," + y(d.name) + ")"; });

  bar.append("rect")
      .attr("width", function(d) { return x(d.value); })
      .attr("height", y.rangeBand());

  bar.append("text")
      .attr("class", "value")
      .attr("x", function(d) { return x(d.value); })
      .attr("y", y.rangeBand() / 2)
      .attr("dx", -3)
      .attr("dy", ".35em")
      .attr("text-anchor", "end")
      .text(function(d) { return format(d.value); })
      .style('font-size', '10px');
    
    d3.select('#modalSvg').append('text')
                .text('Electoral Voting Power by State')
                .attr('transform', 'translate(350,20)')
                .style('font-size', '16px')
                .style('fill', 'white')

  svg.append("g")
      .attr("class", "x axis")
      .attr('transform', 'translate(0,460)')
      .call(xAxis)
        .append('text')
            .text('# of Electoral Votes')
            .attr('transform', 'translate(400,30)')
            .style('font-size', '16px');;

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
            .append('text')
            .text('State')
            .attr('transform', 'translate(-40,255) rotate(-90)')
            .style('font-size', '16px');
});

}
