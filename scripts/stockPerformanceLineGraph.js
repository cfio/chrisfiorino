function stockPerformanceLineGraph() {
    
d3 = d3v3;

var margin = {top: 20, right: 20, bottom: 30, left: 50},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var formatDate = d3.time.format("%d-%b-%y");

var x = d3.time.scale()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var line = d3.svg.line()
    .x(function(d) { return x(d.date); })
    .y(function(d) { return y(d.close); });

var svg = d3.select("#modalSvg").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(100,50) scale(0.8)");

d3.tsv("data/lineGraphData.tsv", type, function(error, data) {
  if (error) throw error;

  x.domain(d3.extent(data, function(d) { return d.date; }));
  y.domain(d3.extent(data, function(d) { return d.close; }));

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
        .append('text')
            .text('Year')
            .attr('transform', 'translate(430,40)')
            .style('font-size', '16px');

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append('text')
            .text('Price ($)')
            .attr('transform', 'translate(-40,255) rotate(-90)')
            .style('font-size', '16px')

  svg.append("path")
      .datum(data)
      .attr("class", "line")
      .attr("d", line);
    
    d3.select('#modalSvg').append('text')
                .text('Stock Price/Share vs. Time')
                .attr('transform', 'translate(370,20)')
                .style('font-size', '16px')
                .style('fill', 'white')
});

function type(d) {
  d.date = formatDate.parse(d.date);
  d.close = +d.close;
  return d;
}
    
}
