function appLogic(jsonData){
    $(document).ready(function() {

	    // Toggle 'Head' buttons on click & expand/hide respective accordions 
        $('.headButton').click(function() {
            var clickedId = $(this).attr('id');
            //var clickedAcc = $("#" + clickedId + "Acc");
            //clickedAcc.children().slideToggle();
            if($(this).hasClass('onHead')) {
                $(this).removeClass('onHead');
            }
            else {
                $(this).addClass('onHead');
            }
            
            if ( $(this).hasClass('analysis') ) {
                $('.analysis').not($(this)).removeClass('onHead');
            }

            if ( $(this).hasClass('context') ) {
                $('.subButton').removeClass('onSub'); 
                $('.context').not($(this)).each(function() {
                    if ( $(this).hasClass('onHead') ) {
                        $(this).removeClass('onHead');
                        $(this).removeClass('active');
                        $(this).siblings('.collapsible-body').stop(true,false).slideUp({ duration: 350, easing: "easeOutQuart", queue: false, complete: function() {$(this).css('height', '');}});
                    }
                 });
            }
        });
   	
    // Toggle accordion buttons on click 
    $('.subButton').click(function() {
        var clickedId = $(this).attr('id');

        $('.subButton').not('#'+clickedId).removeClass('onSub');
                
        if($(this).hasClass('onSub')) {
            $(this).removeClass('onSub');
        }
        else {
            $(this).addClass('onSub');
        }
    });
        
	// Recalculate results when 'filter' class buttons are clicked
    $('.filter').click(function() {
   	    showResults();
	});

	// Recalculate results when keyword search is modified
	$('#vizSearchBox').blur(function() {
	    showResults();
	});

    $('#vizSearchBox').keyup(function() {
        showResults();
    });
    
        
    //Viz Result Icon Mapping
    for ( var item in jsonData ) {
        if ( jsonData.hasOwnProperty(item) ) {
            $('#' + item).find('.vizResultImage').attr('src', '../beta/' + jsonData[item].img);
        }
    }
        
    // Set click listeners on all result options.
	$(".vizResultBar").click(function() {
        var clickedId = $(this).attr('id');
        $('#myModal').css('display', 'block');
        $('#modalTitle').text(jsonData[clickedId].title);
        $('#modalDesc').text(jsonData[clickedId].desc);

	    if ( clickedId != 'radarChart' ) { window[clickedId](); }
	    else { window[clickedId](data, radarChartOptions); }
	});
    
    // *** This section is for autocomplete functionality on keyword search field *** //
    // First obtain a list of all unique keywords from the keywords.json file
    var uniqueKeywords = []; 
    for ( var item in jsonData ) { 
        if ( jsonData.hasOwnProperty(item) ) { 
            for ( var i = 0; i < jsonData[item].keywords.length; i++ ) {
                if ( uniqueKeywords.indexOf(jsonData[item].keywords[i]) == -1 ) {
                    uniqueKeywords.push(jsonData[item].keywords[i]);
                } 
            }
        }
    }
    
    // Capitalize all keywords for pretty display to user 
    var capUniqueKeywords = [];
    for ( var j = 0; j < uniqueKeywords.length; j++ ) {
        // Eliminate special keywords that are in camelcase, these refer to button ID's and shouldn't be displayed in autocomplete
        if ( numberOfUppercaseLetters(titleCase(uniqueKeywords[j])) <= 1 ) {
            capUniqueKeywords.push(titleCase(uniqueKeywords[j]));
        }
    }

    // Build up object to be passed to materialize autocomplete callback
    var autocompleteData = {}; 
    for ( var k = 0; k < capUniqueKeywords.length; k++ ) { 
        autocompleteData[capUniqueKeywords[k]] = null;
    }   
    
    // Pass data object to materialize autocomplete callback
    $('input.autocomplete').autocomplete({
        data: autocompleteData
    }); 
    
    // Show autocomplete when focus in keyword search box
    $('#vizSearchBox').focusin(function() {
        $('.autocomplete-content').show();
    });

    // Close autocomplete when click out of keyword search box
    $('#htmlBody').click(function(e) {
        if ( $(e.target).is('input') || $(e.target).is('li')) {
            e.preventDefault();
            return;
        }
        $('.autocomplete-content').hide();
    });
            
    // *** END AUTOCOMPLETE SECTION *** //

    // Close modal when 'x' is clicked
    $("#close").click(function() {
        $('#myModal').css('display', 'none');    
        $("#d3").children().remove();
	    $("#modalSvg").children().remove();
	});

    // Close modal window when click outside of it
    $(document).click(function(e) {
        var container = $(".modal-content");
        if ( !$(".vizResultBar").is(e.target) && $(".vizResultBar").has(e.target).length === 0 && $("#myModal").is(":visible") && !container.is(e.target) && container.has(e.target).length === 0) {
            $('#myModal').css('display', 'none');
            $("#d3").children().remove();
            $("#modalSvg").children().remove(); 
        }
    });
    });  
}

// @function Apply filter to show results based on search keywords and currently selected buttons
// This is a global function since it is called from this file and materialize.js
function showResults() {

    // Add keyword search bar entries to filter
    var filter = $('#vizSearchBox').val().split(" ");

    // Convert all filter entries to lowercase
    var lowerFilter = [];
    for ( var j = 0; j < filter.length; j++ ) {
        lowerFilter.push(filter[j].toLowerCase());
    }
    filter = lowerFilter; 

    // Populate filter list with id's of all currently clicked buttons
    $('.filter').each(function(){
        if($(this).hasClass('onHead') || $(this).hasClass('onSub')){
            filter.push($(this).attr('id')); 
        }   
    }); 

    // Remove duplicate entries from filter
    filter = filter.filter( function( item, index, inputArray ) { 
        return inputArray.indexOf(item) == index;
    }); 

    // Remove empty strings from filter
    for( var i = 0; i < filter.length; i++ ){
        if (filter[i] === '') filter.splice(i, 1);
    }

    // If no filter is applied, don't display any results
    if ( filter.length == 0 ) {
        for ( var item in jsonData ) {
            if ( jsonData.hasOwnProperty(item) ) {
                $('#' + item).css('display','none');
            }
        }
    }

    for ( var item in jsonData ) {
        if ( jsonData.hasOwnProperty(item) ) {
            if ( arrayContains(filter, jsonData[item].keywords) ) {
                $('#' + item).css('display','flex');
                $('#modalTitle').text(jsonData[item].title);
                $('#modalDesc').text(jsonData[item].desc);
            } else {
                $('#' + item).css('display','none');
            }   
        }   
    }   
}

// @function Return true if all elements in a exist in b, false otherwise.                                                                                                
function arrayContains(a, b) {
    if (a.length == 0) { return false; }
        for ( var i = 0; i < a.length; i++ ) {
            if ( b.indexOf(a[i]) == -1 ) {
                return false;
            }
        }
    return true;
}

// @function Return first letter capitalized version of string
function titleCase(string) {                                                                                                                                            
    return string.charAt(0).toUpperCase() + string.slice(1);
}

// @function Return number of uppercase letters in string
function numberOfUppercaseLetters(string) {
    var numUppercaseLetters = 0;
    for (var i = 0; i < string.length; i++) {
        if ( string[i] == string[i].toUpperCase() ) {
            numUppercaseLetters += 1;
        }
    }
    return numUppercaseLetters;
}
