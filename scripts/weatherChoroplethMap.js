function weatherChoroplethMap() {
d3 = d3v4;
    
var diameter = 960,
    format = d3.format(",d");
    
var svg = d3.select("#modalSvg")
            .append('g')
            .attr('class', 'mapContainer')
            .attr('transform', 'translate(450,240) scale(0.7)');

var rateById = d3.map();

var quantize = d3.scaleQuantize()
    .domain([0, 0.15])
    .range(d3.range(9).map(function(i) { return "w" + i + "-9"; }));

var projection = d3.geoAlbersUsa()
    .scale(1280)
    .translate([+svg.attr("width") / 2, +svg.attr("height") / 2]);

var path = d3.geoPath()
    .projection(projection);

d3.queue()
    .defer(d3.json, "data/weatherChoroUS.json")
    .defer(d3.tsv, "data/weatherChoroUnemployment.tsv", function(d) { rateById.set(d.id, +d.rate); })
    .await(ready);

function ready(error, us) {
  if (error) throw error;

  svg.append("g")
      .attr("class", "counties")
    .selectAll("path")
      .data(topojson.feature(us, us.objects.counties).features)
    .enter().append("path")
      .attr("class", function(d) { return quantize(rateById.get(d.id)); })
      .attr("d", path);

  svg.append("path")
      .datum(topojson.mesh(us, us.objects.states, function(a, b) { return a !== b; }))
      .attr("class", "states")
      .attr("id", "choroPath")
      .attr("d", path);
    
var legendGroup = d3.select('#modalSvg').append('g')
                .attr('transform', 'translate(160,160) scale(0.8)');    
    
legendGroup.append('defs')
            .append('linearGradient')
            .attr('id', 'grad1')
            .attr('x1', '0%')
            .attr('y1', '0%')
            .attr('x2', '0%')
            .attr('y2', '100%')
                
    d3.select('linearGradient')
        .append('stop')
        .attr('offset', '0%')
        .attr('style', 'stop-color:#61100C; stop-opacity:1')
    
    d3.select('linearGradient')
        .append('stop')
        .attr('offset', '50%')
        .attr('style', 'stop-color:#FF443C; stop-opacity:1')
                    
    d3.select('linearGradient')
        .append('stop')
        .attr('offset', '100%')
        .attr('style', 'stop-color:#FFBAB8; stop-opacity:1')
    
legendGroup.append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '14%')
        .attr('height', '34%')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('x', '750px')
        .attr('y', '30px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '40px')
        .style('fill', 'url(#grad1)')
        .attr('width', '5%')
        .attr('height', '30%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '828px')
        .attr('y', '57px')
        .text('0-10in')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('text')
        .attr('x', '828px')
        .attr('y', '96px')
        .text('10-20in')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('text')
        .attr('x', '828px')
        .attr('y', '137px')
        .text('20-30in')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('text')
        .attr('x', '828px')
        .attr('y', '177px')
        .text('30-40in')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
d3.select('#modalSvg').append('text')
                .text('Avg Rainfall in Inches by County')
                .attr('transform', 'translate(370,20)')
                .style('font-size', '16px')
                .style('fill', 'white')

}
}
