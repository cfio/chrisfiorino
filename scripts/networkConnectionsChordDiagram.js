function networkConnectionsChordDiagram() {
    
d3 = d3v4;

// From http://mkweb.bcgsc.ca/circos/guide/tables/
var matrix = [
  [11975,  5871, 8916, 2868],
  [ 1951, 10048, 2060, 6171],
  [ 8010, 16145, 8090, 8045],
  [ 1013,   990,  940, 6907]
];
    
/*d3.select("#d3").append("svg")
    .attr('width', '960px')
    .attr('height', '960px')
*/
    

var svg = d3.select("#modalSvg"),
    width = +svg.attr("width"),
    height = +svg.attr("height"),
    outerRadius = Math.min(width, height) * 0.5 - 40,
    innerRadius = outerRadius - 30;

var formatValue = d3.formatPrefix(",.0", 1e3);

var chord = d3.chord()
    .padAngle(0.05)
    .sortSubgroups(d3.descending);

var arc = d3.arc()
    .innerRadius(innerRadius)
    .outerRadius(outerRadius);

var ribbon = d3.ribbon()
    .radius(innerRadius);

var color = d3.scaleOrdinal()
    .domain(d3.range(4))
    .range(['#42247F', '#5CCC81', '#7F211B', '#2CC7FF']);

var g = svg.append("g")
    .attr("transform", "translate(" + width / 1.6 + "," + height / 1.9 + ") scale(0.9)")
    .datum(chord(matrix));

var group = g.append("g")
    .attr("class", "groups")
  .selectAll("g")
    .data(function(chords) { return chords.groups; })
  .enter().append("g");

group.append("path")
    .style("fill", function(d) { return color(d.index); })
    .style("stroke", function(d) { return d3.rgb(color(d.index)).darker(); })
    .attr("d", arc);

var groupTick = group.selectAll(".group-tick")
    .data(function(d) { return groupTicks(d, 1e3); })
  .enter().append("g")
    .attr("class", "group-tick")
    .attr("transform", function(d) { return "rotate(" + (d.angle * 180 / Math.PI - 90) + ") translate(" + outerRadius + ",0)"; });

groupTick.append("line")
    .attr("x2", 6);

groupTick
  .filter(function(d) { return d.value % 5e3 === 0; })
  .append("text")
    .attr("x", 8)
    .attr("dy", ".35em")
    .attr("transform", function(d) { return d.angle > Math.PI ? "rotate(180) translate(-16)" : null; })
    .style("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
    .text(function(d) { return formatValue(d.value); });

g.append("g")
    .attr("class", "ribbons")
  .selectAll("path")
    .data(function(chords) { return chords; })
  .enter().append("path")
    .attr("d", ribbon)
    .style("fill", function(d) { return color(d.target.index); })
    .style("stroke", function(d) { return d3.rgb(color(d.target.index)).darker(); });
    
    var legendGroup = d3.select('#modalSvg').append('g')
                .attr('transform', 'translate(-420,165) scale(0.8)');
    
    legendGroup.append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '19%')
        .attr('height', '33%')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('x', '750px')
        .attr('y', '30px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '40px')
        .style('fill', '#7F211B')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '57px')
        .text('Friends')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '80px')
        .style('fill', '#42247F')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '97px')
        .text('Family')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '120px')
        .style('fill', '#5CCC81')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '137px')
        .text('Acquaintances')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '160px')
        .style('fill', '#2CC7FF')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '177px')
        .text('Followers')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    d3.select('#modalSvg').append('text')
                .text('Social Network Profile Connections')
                .attr('transform', 'translate(370,20)')
                .style('font-size', '16px')
                .style('fill', 'white')
    

// Returns an array of tick angles and values for a given group and step.
function groupTicks(d, step) {
  var k = (d.endAngle - d.startAngle) / d.value;
  return d3.range(0, d.value, step).map(function(value) {
    return {value: value, angle: value * k + d.startAngle};
  });
}
    
}
