function userAnalysisRadialProgress() {
    
d3 = d3v3;
  
  var width = 960;
  var height = 640;
  
  var outerRadius1 = Math.min(width/2, (2/3) *height);
  var innerRadius1 = outerRadius1 *.85;
  
  var svg = d3.select("#modalSvg")
    .append("svg")
    .attr("class", "arcOne")
    .attr("width", width + "px")
    .attr("height", height + "px")
    .append("g")
    .attr("transform", "translate(460,260) scale(0.5)");

  var arc1 = d3.svg.arc()
  .innerRadius(innerRadius1)
  .outerRadius(outerRadius1)
  .startAngle((-2 / 3) * Math.PI)
  .endAngle((2 / 3) * Math.PI);

  svg.append("path")
    .attr("class", "arc-background1")
    .attr("d", arc1);

  var progress1 = 1;
  var endAngle1 = (progress1 * (4 / 3) * Math.PI) + ((-2 / 3) * Math.PI)

  arc = arc1.endAngle(endAngle1);
  svg.append("path")
    .attr("class", "arc1")
    .attr("d", arc1);

/////
    
var outerRadius2 = Math.min(width/2, (2/3) *height);
var innerRadius2 = outerRadius2 *.85;
    
var svg = d3.select("#modalSvg")
    .append("svg")
    .attr("class", "arcTwo")
    .attr("width", width + "px")
    .attr("height", height + "px")
    .append("g")
    .attr("transform", "translate(460,260) scale(0.4)");
    
var arc2 = d3.svg.arc()
  .innerRadius(innerRadius2)
  .outerRadius(outerRadius2)
  .startAngle((-2 / 3) * Math.PI)
  .endAngle((2 / 3) * Math.PI);

  svg.append("path")
    .attr("class", "arc-background2")
    .attr("d", arc2);

  var progress2 = .80;
  var endAngle2 = (progress2 * (4 / 3) * Math.PI) + ((-2 / 3) * Math.PI)

  arc = arc.endAngle(endAngle2);
  svg.append("path")
    .attr("class", "arc2")
    .attr("d", arc1);
    
/////
    
var outerRadius3 = Math.min(width/2, (2/3) *height);
var innerRadius3 = outerRadius3 *.85;
    
var svg = d3.select("#modalSvg")
    .append("svg")
    .attr("class", "arcThree")
    .attr("width", width + "px")
    .attr("height", height + "px")
    .append("g")
    .attr("transform", "translate(460,260) scale(0.316)");
    
var arc3 = d3.svg.arc()
  .innerRadius(innerRadius3)
  .outerRadius(outerRadius3)
  .startAngle((-2 / 3) * Math.PI)
  .endAngle((2 / 3) * Math.PI);

  svg.append("path")
    .attr("class", "arc-background3")
    .attr("d", arc2);

  var progress3 = .67;
  var endAngle3 = (progress3 * (4 / 3) * Math.PI) + ((-2 / 3) * Math.PI)

  arc = arc.endAngle(endAngle3);
  svg.append("path")
    .attr("class", "arc3")
    .attr("d", arc1);
    
/////    
    
  svg.append('text')
    .attr('text-anchor', 'middle')
    .attr('y', -100)
    .attr('id', 't1')
    .text('1.2M');
    
  svg.append('text')
    .attr('text-anchor', 'middle')
    .attr('y', -10)
    .attr('id', 't2')
    .text('800K');
    
  svg.append('text')
    .attr('text-anchor', 'middle')
    .attr('y', 80)
    .attr('id', 't3')
    .text('400K');
    
 d3.select('#modalSvg').append('text')
                .text("Application X User Base Analysis")
                .attr('transform', 'translate(360,20)')
                .style('font-size', '16px')
                .style('fill', 'white')

 var legendGroup = d3.select('#modalSvg').append('g')
                .attr('transform', 'translate(100,150) scale(0.8)');    
    
legendGroup.append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '20%')
        .attr('height', '25%')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('x', '750px')
        .attr('y', '30px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '40px')
        .style('fill', '#2CC7FF')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '57px')
        .text('Total Users')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '80px')
        .style('fill', '#73FFA2')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '97px')
        .text('Daily Active Users')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '120px')
        .style('fill', '#FF8A83')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '137px')
        .text('New Users')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
 
 
  }


