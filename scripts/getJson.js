var jsonData = undefined;
$(document).ready(function() {
    
    var xmlhttp = new XMLHttpRequest();
    var url = "data/keywords.json";
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var json = JSON.parse(xmlhttp.responseText);
            appLogic(json);
            jsonData = json;     
        }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    
});
