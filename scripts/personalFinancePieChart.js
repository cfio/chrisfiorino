function personalFinancePieChart() {
    
d3 = d3v3;

var width = 960,
    height = 500,
    radius = Math.min(width, height) / 2;

var color = d3.scale.ordinal()
    .range(['#78DBFF', '#2CC7FF', '#239FCC', '#B895FF', '#FF8A83', '#3C6E7F', '#5CCC81']);

var arc = d3.svg.arc()
    .outerRadius(radius - 10)
    .innerRadius(0);

var labelArc = d3.svg.arc()
    .outerRadius(radius - 40)
    .innerRadius(radius - 40);

var pie = d3.layout.pie()
    .sort(null)
    .value(function(d) { return d.dollars; });

var svg = d3.select("#modalSvg").append("svg")
    .attr("width", width)
    .attr("height", height)
  .append("g")
    .attr("transform", "translate(450,250) scale(0.8)");

d3.csv("data/personalFinanceDonutChartData.csv", type, function(error, data) {
  if (error) throw error;

  var g = svg.selectAll(".arc")
      .data(pie(data))
    .enter().append("g")
      .attr("class", "arc");

  g.append("path")
      .attr("d", arc)
      .style("fill", function(d) { return color(d.data.dollars); });

  g.append("text")
      .attr("transform", function(d) { return "translate(" + labelArc.centroid(d) + ")"; })
      .attr("dy", ".35em")
      .style("fill", "#FFFFFF")
      .style("font-weight", "bold")
      .text(function(d) { return "$"+String(d.data.dollars); })
      .style('font-size', '12px'); 


    
var legendGroup = d3.select('#modalSvg').append('g')
                        .attr("transform", "translate(170,130) scale(0.7)");
    
    legendGroup.append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '19%')
        .attr('height', '57%')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('x', '750px')
        .attr('y', '30px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '40px')
        .style('fill', '#78DBFF')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '57px')
        .text('Shopping')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '80px')
        .style('fill', '#2CC7FF')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '97px')
        .text('Entertainment')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '120px')
        .style('fill', '#239FCC')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '137px')
        .text('Food')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '160px')
        .style('fill', '#B895FF')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '177px')
        .text('Bills & Utilities')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '200px')
        .style('fill', '#FF8A83')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '217px')
        .text('Housing')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '240px')
        .style('fill', '#3C6E7F')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '257px')
        .text('Auto & Transport')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
     legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '280px')
        .style('fill', '#5CCC81')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '297px')
        .text('Education')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    d3.select('#modalSvg').append('text')
                .text('Monthly Spending by Budget Category')
                .attr('transform', 'translate(330,20)')
                .style('font-size', '16px')
                .style('fill', 'white')
    
});

function type(d) {
  d.dollars = +d.dollars;
  return d;
}

}
