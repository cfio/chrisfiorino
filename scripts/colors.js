//COLORS

//Blue Palette

1   '#78DBFF'
2   '#2CC7FF'
3   '#239FCC'
4   '#3C6E7F'
5   '#16647F'

//Green Palette

1   '#C0FFD5'
2   '#73FFA2'
3   '#5CCC81'
4   '#607F6A'
5   '#3A7F51'

//Purple Palette

1   '#B895FF'
2   '#8449FF'
3   '#6A3ACC'
4   '#5C4B7F'
5   '#42247F'

//Red Palette

1   '#FF8A83'
2   '#FF4236'
3   '#CC352B'
4   '#7F4541'
5   '#7F211B'

//Orange Palette

1   '#ffcc80'
2   '#ffb74d'
3   '#fb8c00'
4   '#f57c00'
5   '#e65100'

//Green Gradient

    '#DDFFE5'
    '#BFFFCD'
    '#9EFFB2'
    '#81FF9A'
    '#4AC255'
    '#4AC255'
    '#348A3C'
    '#28692E'
    '#1B471F'
    
//Red Gradient
    
    '#FFBAB8'
    '#FFA09C'
    '#FF7E74'
    '#FF6059'
    '#FF443C'
    '#FF2A20'
    '#B81E17'
    '#911812'
    '#61100C'
    
    
    
    
    