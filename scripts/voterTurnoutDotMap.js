function voterTurnoutDotMap() {
    
d3 = d3v3;

var width = 960,
    height = 500;

var radius = d3.scale.sqrt()
    .domain([0, 1e6])
    .range([0, 10]);

var path = d3.geo.path();

var svg = d3.select("#modalSvg")
            .append('g')
            .attr('class', 'svgContainer')
            .attr('transform', 'translate(40,30) scale(0.8)');

queue()
    .defer(d3.json, "data/dotMapUS.json")
    .defer(d3.json, "data/us-state-centroids.json")
    .await(ready);

function ready(error, us, centroid) {
  if (error) throw error;

  svg.append("path")
      .attr("class", "statesDot")
      .datum(topojson.feature(us, us.objects.states))
      .attr("d", path);

  svg.selectAll(".symbol")
      .data(centroid.features.sort(function(a, b) { return b.properties.population - a.properties.population; }))
    .enter().append("path")
      .attr("class", "symbol")
      .attr("d", path.pointRadius(function(d) { return radius(d.properties.population); }));
}
    
var legendGroup = d3.select('.svgContainer').append('g')
                .attr('transform', 'translate(300,160) scale(0.8)');    
    
legendGroup.append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '22%')
        .attr('height', '55%')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('x', '710px')
        .attr('y', '20px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    legendGroup.append('circle')
        .attr('class', 'symbol')
        .attr('cx', '780px')
        .attr('cy', '60px')
        .attr('r', '10px')
    
    legendGroup.append('text')
        .attr('x', '850px')
        .attr('y', '65px')
        .text('20%')
        .style('font-size', '14pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('circle')
        .attr('class', 'symbol')
        .attr('cx', '780px')
        .attr('cy', '120px')
        .attr('r', '25px')
    
    legendGroup.append('text')
        .attr('x', '850px')
        .attr('y', '125px')
        .text('40%')
        .style('font-size', '14pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('circle')
        .attr('class', 'symbol')
        .attr('cx', '780px')
        .attr('cy', '220px')
        .attr('r', '50px')
    
    legendGroup.append('text')
        .attr('x', '850px')
        .attr('y', '220px')
        .text('80%')
        .style('font-size', '14pt')
        .style('fill', '#FFFFFF')
    
d3.select('#modalSvg').append('text')
                .text('Voter Turnout % by State Capital')
                .attr('transform', 'translate(350,20)')
                .style('font-size', '16px')
                .style('fill', 'white')
    
}