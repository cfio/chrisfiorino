function revenueBreakdownStackedAreaChart() {
    
d3 = d3v4;

var svg = d3.select("#modalSvg"),
    margin = {top: 20, right: 20, bottom: 30, left: 50},
    width = svg.attr("width") - margin.left - margin.right,
    height = svg.attr("height") - margin.top - margin.bottom;

var parseDate = d3.timeParse("%Y %b %d");

var x = d3.scaleTime().range([0, width]),
    y = d3.scaleLinear().range([height, 0]),
    z = d3.scaleOrdinal(['#7F211B', '#5C4B7F', '#2CC7FF', '#5CCC81', '#B895FF', '#FF8A83']);

var stack = d3.stack();

var area = d3.area()
    .x(function(d, i) { return x(d.data.date); })
    .y0(function(d) { return y(d[0]); })
    .y1(function(d) { return y(d[1]); });

var g = svg.append("g")
    .attr("transform", "translate(80,50) scale(0.9)");

d3.tsv("data/stackedAreaChartData.tsv", type, function(error, data) {
  if (error) throw error;
  var keys = data.columns.slice(1);

  x.domain(d3.extent(data, function(d) { return d.date; }));
  z.domain(keys);
  stack.keys(keys);

  var layer = g.selectAll(".layer")
    .data(stack(data))
    .enter().append("g")
      .attr("class", "layer");

  layer.append("path")
      .attr("class", "area")
      .style("fill", function(d) { return z(d.key); })
      .attr("d", area);

  layer.filter(function(d) { return d[d.length - 1][1] - d[d.length - 1][0] > 0.01; })
    .append("text")
      .attr("x", width - 6)
      .attr("y", function(d) { return y((d[d.length - 1][0] + d[d.length - 1][1]) / 2); })
      .attr("dy", ".35em")
      .style("font", "10px sans-serif")
      .style("text-anchor", "end")
      .style('fill', '#FFFFFF')
      .text(function(d) { return d.key; });

  g.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x))
            .append('text')
            .text('Month')
            .attr('transform', 'translate(440,40)')
            .style('font-size', '14px');

  g.append("g")
      .attr("class", "axis axis--y")
      .call(d3.axisLeft(y).ticks(10, "%"))
            .append('text')
            .text('Revenue %')
            .attr('transform', 'translate(-40,195) rotate(-90)')
            .style('font-size', '14px');
    
d3.select('#modalSvg').append('text')
                .text('Product Revenue Breakdown by Month')
                .attr('transform', 'translate(340,20)')
                .style('font-size', '16px')
                .style('fill', 'white')
    
});

function type(d, i, columns) {
  d.date = parseDate(d.date);
  for (var i = 1, n = columns.length; i < n; ++i) d[columns[i]] = d[columns[i]] / 100;
  return d;
}

    
}
