// Bubble Chart Visualization
function campaignFinanceBubbleChart() {
d3 = d3v3;
    
var diameter = 960,
    format = d3.format(",d"),
    color = d3.scale.ordinal()
    .range([   //'#78DBFF',
               '#2CC7FF',
               //'#239FCC',
               '#42247F',
               '#FF4236'//,
               //'#CC352B'
               ]);

var bubble = d3.layout.pack()
    .sort(null)
    .size([960, 500])
    .padding(1.5);

var svg = d3.select("#modalSvg").append("svg")
    .attr("width", diameter)
    .attr("height", diameter)
    .attr("class", "bubble")
    .append("g")
    .attr("transform", "translate(-60,40) scale(0.9)");
    

d3.json("data/flare.json", function(error, root) {
  if (error) throw error;

  var node = svg.selectAll(".node")
      .data(bubble.nodes(classes(root))
      .filter(function(d) { return !d.children; }))
    .enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

  node.append("title")
      .text(function(d) { return d.className + ": $" + String(d.value) + " k"; });

  node.append("circle")
      .attr("r", function(d) { return d.r; })
      .style("fill", function(d) { return color(d.packageName); })
      .style('fill-opacity', .8)
     .style('stroke', '#FFFFFF')
     .style('stroke-opacity', 1);

  node.append("text")
      .attr("dy", ".3em")
      .style("text-anchor", "middle")
      .style("fill", "#FFFFFF")
      .style("font-size", "1em")
      .style("font-weight","bold")
      .text(function(d) { return d.className.substring(0, d.r / 3); });
});
    
    var legendGroup = d3.select('#modalSvg').append('g')
                .attr('transform', 'translate(0,170) scale(0.8)');    
    
legendGroup.append('rect')
        .attr('class', 'vertBarLegend')
        .attr('width', '17%')
        .attr('height', '25%')
        .style('fill', '#FFFFFF')
        .style('fill-opacity', .2)
        .style('stroke', '#FFFFFF')
        .style('stroke-opacity', 1)
        .attr('x', '750px')
        .attr('y', '30px')
        .attr('rx', '5px')
        .attr('ry', '5px')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '40px')
        .style('fill', '#2CC7FF')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '57px')
        .text('Democrat')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '80px')
        .style('fill', '#FF4236')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '97px')
        .text('Republican')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
    legendGroup.append('rect')
        .attr('class', 'color')
        .attr('x', '760px')
        .attr('y', '120px')
        .style('fill', '#42247F')
        .attr('width', '5%')
        .attr('height', '5%')
        .attr('rx', '2px')
        .attr('ry', '2px')
    
    legendGroup.append('text')
        .attr('x', '818px')
        .attr('y', '137px')
        .text('Independent')
        .style('font-size', '10pt')
        .style('fill', '#FFFFFF')
    
d3.select('#modalSvg').append('text')
                .text('Campaign Contribution Sources ($K) Received per Political Party')
                .attr('transform', 'translate(240,20)')
                .style('font-size', '16px')
                .style('fill', 'white')
    
    

// Returns a flattened hierarchy containing all leaf nodes under the root.
function classes(root) {
  var classes = [];

  function recurse(name, node) {
    if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
    else classes.push({packageName: name, className: node.name, value: node.size});
  }

  recurse(null, root);
  return {children: classes};
}

d3.select(self.frameElement).style("height", diameter + "px");}
//Bubble Chart END
