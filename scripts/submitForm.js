$(document).ready(function() {

    function submitForm() {    
        
        // Extract user inputted form values from fields
        var formFields = {
            'name': $('#name').val(),
            'phone': $('#phone_number').val(),
            'company': $('#company_name').val(),
            'email': $('#email').val(),
            'website': $('#website').val(),
            'type': $('#project_type').val(),
            'budget': $('#project_budget').val(),
            'desc': $('#form_description').val()
        };

        // Create new XML HTTP Request object and define URL to CGI Script
        var xmlhttp = new XMLHttpRequest();
        var url = "https://www.analytixbar.com/beta/cgi-bin/sendContactFormEmail.py";
        
        // Construct URL parameters and add to URL
        var isFirstParam = true;
        for ( prop in formFields ) {
            if ( formFields.hasOwnProperty(prop) ) {
                if ( formFields[prop] ) {
                    if ( isFirstParam ) {
                        url += "?";
                        url += prop + "=" + formFields[prop];
                        isFirstParam = false;
                    } else {
                        url += "&" + prop + "=" + formFields[prop];
                    }
                }
            }
        }
        
        // Ensure that minimum requirements for fields are filled out before allowing submission.
        if ( !(formFields.name && (formFields.phone || formFields.email)) ) {
            alert("Please fill out your name and at least one method of contact before submitting the form.");
            return false;
        }

        // Validate email address if filled in
        if ( formFields.phone == '' && !isValidEmailAddress(formFields.email) ) {
            alert("Please enter a valid email address and resubmit the form.");
            return false;
        }

        // Finalize and send request
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
        
        // Reset contact form fields to empty
        $('#name').removeClass('valid').removeClass('invalid').val('').text('');
        $('#phone_number').removeClass('valid').removeClass('invalid').val('').text('');
        $('#company_name').removeClass('valid').removeClass('invalid').val('').text('');
        $('#email').removeClass('valid').removeClass('invalid').val('').text('');
        $('#website').removeClass('valid').removeClass('invalid').val('').text('');
        $('#project_type').removeClass('valid').removeClass('invalid').val('').text('');
        $('#project_budget').removeClass('valid').removeClass('invalid').val('').text('');
        $('#form_description').removeClass('valid').removeClass('invalid').val('').text('');
        $('.clearOnFormSubmit').removeClass('active');
        alert("Thank you for contacting AnalytixBar! We will review your information and get back to you within 24 hours!\n\nBest,\nThe AxBar Team");
        return false;                   
    }

    // Run the function above when the form is submitted by the end user on the page
    $('#contactForm').submit(function () {
        submitForm();
        return false;
    });

    function isValidEmailAddress(email) {
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    }

});
