function verbalizeLogic(jsonData){
    $(document).ready(function() {
		for(var item in jsonData) {
			if (jsonData.hasOwnProperty(item)){
				$("#unitCategory").append("<option>" + item + "</option>"); 
			};
		};  
	
		$("#unitCategory").change(function() {
			populateUnits();
		});

		$("#processButton").click(function() {
			generateResults();
	  	});
  	})
}

function populateUnits(){
	unitSelected = $("#unitCategory").val();
	var counter = 0;
	//Loop through categories to find user selection
	for(var item in jsonData) {
		if (jsonData.hasOwnProperty(item)){
			if (unitSelected == item) {
				$("#inputUnit").html("");
		    	//Loop though the available options, add to second dropdown menu
				for (var i = 0; i < jsonData[item].units.length; i++) {
					$("#inputUnit").append("<option>" + jsonData[item].units[i].name + "</option>"); 
				};
				break;
			};
		}
	};
}

function generateResults(){
	$("#convertedResults").html("");
	unitSelected = $("#unitCategory").val();
	inputUnit = $("#inputUnit").val();
	inputNumber = $("#inputNumber").val();
	var counter = 0;
	//Loop through categories to find user selection
	for(var item in jsonData) {
		if (jsonData.hasOwnProperty(item)){
			if (unitSelected == item) {
				//Loop though the available options, add to second dropdown menu
				for (var i = 0; i < jsonData[item].units.length; i++) {
					if (inputUnit ==  jsonData[item].units[i].name) {
						for(var j = 0; j < jsonData[item].outputunits.length; j++) {
							var numberOf = 0;
							numberOf = (inputNumber * jsonData[item].units[i].basefactor) / ( jsonData[item].outputunits[j].basefactor );
							$("#convertedResults").append("<li> Is equal to " + numberWithCommas(numberOf.toFixed(1)) + " " + jsonData[item].outputunits[j].name + "</li>");  
						};
					};
				};
				break;
			};
		}
	};
}
function numberWithCommas(x){
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

