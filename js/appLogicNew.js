// Toggle 'Head' buttons on click & expand/hide respective accordions 
        $('.headButton').click(function() {
            var clickedId = $(this).attr('id');
            var clickedAcc = $("#" + clickedId + "Acc");
            clickedAcc.children().slideToggle();
            if($(this).hasClass('onHead')) {
                $(this).removeClass('onHead');
            }
            else {
                $(this).addClass('onHead');
            }
            
            if ( $(this).hasClass('analysis') ) {
                $(this).siblings().removeClass('onHead');
            }

            if ( $(this).hasClass('context') ) {
                var buttonRow = undefined;
                if ( $(this).hasClass('firstRowButton') ) { buttonRow = 1; }
                if ( $(this).hasClass('secondRowButton') ) { buttonRow = 2; }
                $('.subButton').removeClass('onSub'); 
                $('.context').not($(this)).each(function() {
                    if ( $(this).hasClass('onHead') ) {
                        $(this).removeClass('onHead');
                        var dataId = $(this).attr('id');
                        var dataAcc = $('#'+dataId+'Acc');
                        dataAcc.children().slideToggle();
                    }
                 });
            }
        });
   	
    // Toggle accordion buttons on click 
    $('.subButton').click(function() {
        var clickedId = $(this).attr('id');

        $('.subButton').not('#'+clickedId).removeClass('onSub');
                
        if($(this).hasClass('onSub')) {
            $(this).removeClass('onSub');
        }
        else {
            $(this).addClass('onSub');
        }
    });
        
	// Recalculate results when 'filter' class buttons are clicked
    $('.filter').click(function() {
   	    showResults();
	});

	// Recalculate results when keyword search is modified
	$('#vizSearchBox').blur(function() {
	    showResults();
	});

 	// @function Apply filter to show results based on search keywords and currently selected buttons
	function showResults() {

	    // Add keyword search bar entries to filter
	    var filter = $('#vizSearchBox').val().split(" ");

	    // Populate filter list with id's of all currently clicked buttons
        $('.filter').each(function(){
            if($(this).hasClass('onHead') || $(this).hasClass('onSub')){
                filter.push($(this).attr('id')); 
            }
        });
	    
	    // Remove duplicate entries from filter
	    filter = filter.filter( function( item, index, inputArray ) {
	        return inputArray.indexOf(item) == index;
	    });

	    // Remove empty strings from filter
	    for( var i = 0; i < filter.length; i++ ){
		if (filter[i] === '') filter.splice(i, 1);
	    }
	   
	    // If no filter is applied, don't display any results
        if ( filter.length == 0 ) {
            for ( var item in jsonData ) {
                if ( jsonData.hasOwnProperty(item) ) {
                    $('#' + item).css('display','none');
                }
            }
        }
    
        for ( var item in jsonData ) {
            if ( jsonData.hasOwnProperty(item) ) {
                if ( arrayContains(filter, jsonData[item].keywords) ) {
                    $('#' + item).css('display','flex');
                    $('#modalTitle').text(jsonData[item].title);
		        	$('#modalDesc').text(jsonData[item].desc);
		        } else {
                    $('#' + item).css('display','none');
                }
            }
        }
	}
        
    //Viz Result Icon Mapping
    for ( var item in jsonData ) {
        if ( jsonData.hasOwnProperty(item) ) {
            $('#' + item).find('.vizResultImage').attr('src', '../beta/' + jsonData[item].img);
        }
    }
        
    
    // @function Return true if all elements in a exist in b, false otherwise.
    function arrayContains(a, b) {
        if (a.length == 0) { return false; }
        for ( var i = 0; i < a.length; i++ ) {
            if ( b.indexOf(a[i]) == -1 ) {
                return false;
            }
        }
        return true;
    }
            
    // Set click listeners on all result options.
	$(".vizResultBar").click(function() {
	    var clickedId = $(this).attr('id');
	    $('#myModal').css('display', 'block');
        $('#modalTitle').text(jsonData[clickedId].title);
        $('#modalDesc').text(jsonData[clickedId].desc);

	    if ( clickedId != 'radarChart' ) { window[clickedId](); }
	    else { window[clickedId](data, radarChartOptions); }
	});

        // When the user clicks on <span> (x), close the modal
        $("#close").click(function() {
            $('#myModal').css('display', 'none');    
            $("#d3").children().remove();
	    $("#modalSvg").children().remove();
	});

    // Close modal window when click outside of it
    $(document).click(function(e) {
        
        var container = $(".modal-content");

        if ( !$(".vizResultBar").is(e.target) && $(".vizResultBar").has(e.target).length === 0 && $("#myModal").is(":visible") && !container.is(e.target) && container.has(e.target).length === 0) {
            $('#myModal').css('display', 'none');
            $("#d3").children().remove();
            $("#modalSvg").children().remove(); 
        }

    });
    
    });  
}