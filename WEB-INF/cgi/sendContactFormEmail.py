#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# enable debugging
import cgitb, cgi, smtplib, datetime
cgitb.enable()

fs = cgi.FieldStorage()
print "Content-Type: text/plain;charset=utf-8"

f = open('contactFormSubmissionHistory.txt', 'a')
now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
f.write("\n\nSubmission Timestamp: " + now + "\n")
content = "\n"
for key in fs.keys():
    f.write("%s = %s" % (key, fs[key].value))
    f.write("\n")
    content += key + ": " + fs[key].value + "\n"

f.close()

fromaddr = 'analytix.bar@gmail.com'
toaddrs  = 'info@analytixbar.com'

msg = "\r\n".join([
    "From: analytix.bar@gmail.com",
    "To: info@analytixbar.com",
    "Subject: New Contact Form Submission"
    "",
    content
])

username = 'analytix.bar@gmail.com'
password = 'ch33seFri3s'
server = smtplib.SMTP('smtp.gmail.com:587')
server.ehlo()
server.starttls()
server.login(username,password)
server.sendmail(fromaddr, toaddrs, msg)
server.quit()
